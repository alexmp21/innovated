const newLocal = require('@tailwindcss/custom-forms');
module.exports = {
  purge: ['./src/**/*.{html,ts}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'facted': {
            '50': '#fef4f4',
            '100': '#fde9e9',
            '200': '#f9c8c8',
            '300': '#f6a7a7',
            '400': '#ef6666',
            '500': '#e82424',
            '600': '#d12020',
            '700': '#ae1b1b',
            '800': '#8b1616',
            '900': '#721212',
            '1000': '#F5F5F5',
            '3000': '#33985A',
            '4000': '#036A96',


        },
        'shark': {
            '50': '#f4f4f4',
            '100': '#e9e9e9',
            '200': '#c8c8c8',
            '300': '#a7a7a7',
            '400': '#646464',
            '500': '#242424',
            '600': '#202020',
            '700': '#1b1b1b',
            '800': '#161616',
            '900': '#121212'
        },


        'celeste': {
          500: '#2DA6FE',
          600: '#00C1AA',
          700: '#8B6CE3',
          750: '#6DD230',
          800: '#E78E09',
          900: '#508EC8',

      },

    }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [ require("@tailwindcss/custom-forms"),
    newLocal
  ],
};
