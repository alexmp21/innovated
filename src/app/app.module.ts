import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DemoMaterialModule } from './material-module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


import { MAT_DATE_LOCALE } from '@angular/material/core'; // Calendario En español
import { AdminModule } from './admin/admin.module';
import { NavbarwebComponent } from './web/shared/navbarweb/navbarweb.component';
import { SidebarwebComponent } from './web/shared/sidebarweb/sidebarweb.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarwebComponent,
    SidebarwebComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    BrowserModule,
    RouterModule,
    CommonModule,
    NgxSpinnerModule,
    AdminModule

  ],

  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-PE'}], // modulo nesesario para cambiar el idioma detucalendario
  bootstrap: [AppComponent]
})

export class AppModule { }
