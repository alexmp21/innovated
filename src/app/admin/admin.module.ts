import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './auth/login/login.component';
import { ManualComponent } from './pages/manual/manual.component';
import { ControlComponent } from './pages/control/control.component';
import { ListadoComponent } from './pages/listado/listado.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { AdminComponent } from './admin.component';
import { RouterModule } from '@angular/router';
import { DemoMaterialModule } from '../material-module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { VerDetalleComponent } from './pages/control/modalesControl/ver-detalle/ver-detalle.component';
import { VerContizacionComponent } from './pages/control/modalesControl/ver-contizacion/ver-contizacion.component';
import { VersionFactesolComponent } from './pages/control/modalesControl/version-factesol/version-factesol.component';
import { LoadingComponent } from './components/loading/loading.component';
import { ResumenContizacionComponent } from './pages/control/modalesControl/resumen-contizacion/resumen-contizacion.component';
import { GuardarsuccessComponent } from './components/guardarsuccess/guardarsuccess.component';
import { CancelComponent } from './components/cancel/cancel.component';
import { VerificarComponent } from './pages/control/modalesControl/verificar/verificar.component';
import { AceptarContratoComponent } from './pages/control/modalesControl/aceptar-contrato/aceptar-contrato.component';
import { EnviarCotizacionComponent } from './pages/manual/enviar-cotizacion/enviar-cotizacion.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentosComponent } from './pages/listado/documentos/documentos.component';
import { VerMascontactosComponent } from './pages/listado/ModalesContactos/ver-mascontactos/ver-mascontactos.component';
import { ContactoComponent } from './pages/listado/ModalesContactos/contacto/contacto.component';
import { NuevoContactoComponent } from './pages/listado/ModalesContactos/nuevo-contacto/nuevo-contacto.component';
import { AgregaDeudaComponent } from './pages/listado/modalPagosDeudas/agrega-deuda/agrega-deuda.component';
import { SuspenderServiciosComponent } from './pages/listado/modalPagosDeudas/suspender-servicios/suspender-servicios.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { VerDetallePagoComponent } from './pages/listado/modalPagosDeudas/ver-detalle-pago/ver-detalle-pago.component';
import { EditarPagoComponent } from './pages/listado/modalPagosDeudas/editar-pago/editar-pago.component';
import { PasosPagosComponent } from './pages/listado/modalPagosDeudas/pasos-pagos/pasos-pagos.component';





@NgModule({

  declarations: [
    LoginComponent,
    ManualComponent,
    ControlComponent,
      VerDetalleComponent,
      VerContizacionComponent,
      VersionFactesolComponent,
    ListadoComponent,
    NavbarComponent,
    SidebarComponent,
    AdminComponent,
    LoadingComponent,
    ResumenContizacionComponent,
    GuardarsuccessComponent,
    CancelComponent,
    VerificarComponent,
    AceptarContratoComponent,
    EnviarCotizacionComponent,
    DocumentosComponent,
    VerMascontactosComponent,
    ContactoComponent,
    NuevoContactoComponent,
    AgregaDeudaComponent,
    SuspenderServiciosComponent,
    SpinnerComponent,
    VerDetallePagoComponent,
    EditarPagoComponent,
    PasosPagosComponent,



  ],
    exports:[
      LoginComponent,
      ManualComponent,
      ControlComponent,
      ListadoComponent,
      NavbarComponent,
      SidebarComponent,
      AdminComponent

    ],

  imports: [
    CommonModule,
    RouterModule,
    DemoMaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule


  ]
})
export class AdminModule { }
