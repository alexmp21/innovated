import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cancel',
  templateUrl: './cancel.component.html',
  styles: [
  ]
})
export class CancelComponent implements OnInit {

  constructor() { }

@Input() cancel: boolean = false;

// MODAL DE CANCELAR
modalcancel(){
  this.cancel = !this.cancel;
}

  ngOnInit(): void {
  }

}
