import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-guardarsuccess',
  templateUrl: './guardarsuccess.component.html',
  styles: [
  ]
})
export class GuardarsuccessComponent implements OnInit {

  @Input() guardar:boolean = false;
  modalguardar(){
    this.guardar =!this.guardar;
    this.Settimeout_msexito();
  }


  Settimeout_msexito() {
    setTimeout(() => {
      this.guardar = false;
    }, 2000);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
