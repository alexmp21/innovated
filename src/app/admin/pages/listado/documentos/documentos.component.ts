import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VerMascontactosComponent } from '../ModalesContactos/ver-mascontactos/ver-mascontactos.component';
import { ContactoComponent } from '../ModalesContactos/contacto/contacto.component';
import { NuevoContactoComponent } from '../ModalesContactos/nuevo-contacto/nuevo-contacto.component';
import { AgregaDeudaComponent } from '../modalPagosDeudas/agrega-deuda/agrega-deuda.component';
import { SuspenderServiciosComponent } from '../modalPagosDeudas/suspender-servicios/suspender-servicios.component';
import { VerDetallePagoComponent } from '../modalPagosDeudas/ver-detalle-pago/ver-detalle-pago.component';
import { EditarPagoComponent } from '../modalPagosDeudas/editar-pago/editar-pago.component';
import { PasosPagosComponent } from '../modalPagosDeudas/pasos-pagos/pasos-pagos.component';


@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styles: [
  ]
})
export class DocumentosComponent implements OnInit {

  constructor(public dialog:MatDialog) { }

// modals ver mas contactos
  vermascontactos(){
    this.dialog.open(VerMascontactosComponent);
  }
// modals inforacion del contacto
  contactos(){
    this.dialog.open(ContactoComponent);
  }
// modals ver mas contactos
  nuevocontacto(){
    this.dialog.open(NuevoContactoComponent);
  }

// modal agregar Deuda
  AgregarDeuda(){
    this.dialog.open(AgregaDeudaComponent);
  }

// modal suspender Servicio
  suspenderservicio(){
    this.dialog.open(SuspenderServiciosComponent);
  }

//modal de detalle Pago
  verDetallePago(){
    this.dialog.open(VerDetallePagoComponent);
  }

//modal editar Pago
editarpago(){
  this.dialog.open(EditarPagoComponent);
}

//moda paso para  pagos

pasospagos(){
  this.dialog.open(PasosPagosComponent);
}




  openTab = 1;
  toggleTabs($tabNumber: number){
    this.openTab = $tabNumber;
  }

  openTabs = 1;
  toggleTab($tabNumber: number){
    this.openTabs = $tabNumber;
  }

  ngOnInit(): void {
  }




}
