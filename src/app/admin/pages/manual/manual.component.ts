import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { EnviarCotizacionComponent } from './enviar-cotizacion/enviar-cotizacion.component';

@Component({
  selector: 'app-manual',
  templateUrl: './manual.component.html',
  styles: [
  ]
})
export class ManualComponent implements OnInit {

  public elegirDatosfacTEDSOL:boolean;
  public elegirDatosfactuTED:boolean;
  public mostarDatos:boolean;
  constructor(public dialog:MatDialog) {
    this.mostarDatos = false;
    this.elegirDatosfacTEDSOL = false;
    this.elegirDatosfactuTED = false;
  }
enviarcorreo(){
  this.dialog.open(EnviarCotizacionComponent);
}


  ngOnInit(): void {
  }
  onShowHide(){
    this.mostarDatos =true; 
  }
  onElegirFacTEDSOL(){
    this.elegirDatosfacTEDSOL =true;
    this.elegirDatosfactuTED =false;
  }
  onElegirFactuTED(){
    this.elegirDatosfactuTED =true;
    this.elegirDatosfacTEDSOL =false;
  }

}
