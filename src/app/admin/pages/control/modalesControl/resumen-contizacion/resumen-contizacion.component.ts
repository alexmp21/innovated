import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resumen-contizacion',
  templateUrl: './resumen-contizacion.component.html',
  styles: [],
})
export class ResumenContizacionComponent implements OnInit {
  constructor(public dialog:MatDialog) {}

  cancelar: boolean = false;
  guardar: boolean = false;

  cancel() {
    this.cancelar = !this.cancelar;
  }

  success() {
    this.guardar = !this.guardar;
    this.Settimeout_msexito();
  }

  // ModalTime
Settimeout_msexito() {
  setTimeout(() => {
    this.dialog.closeAll();
    this.guardar = false;
  }, 2000);
}


  ngOnInit(): void {}
}
