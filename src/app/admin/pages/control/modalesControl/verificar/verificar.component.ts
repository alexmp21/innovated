import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { AceptarContratoComponent } from '../aceptar-contrato/aceptar-contrato.component';

@Component({
  selector: 'app-verificar',
  templateUrl: './verificar.component.html',
  styles: [
  ]
})
export class VerificarComponent implements OnInit {

  loading: boolean = false;
  mensaje: string ="Generando Base de Datos..";

  loadingAceptar() {
    this.loading = !this.loading;
    this.Settimeout_msexito();
  }

  // ModalTime
  Settimeout_msexito() {
    setTimeout(() => {
      this.loading = false;
      this.aceptar();
    }, 3000);
  }


aceptar(){
  this.dialog.open(AceptarContratoComponent);
}

  constructor(public dialog:MatDialog) { }

  ngOnInit(): void {
  }

}
