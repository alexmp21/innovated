import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aceptar-contrato',
  templateUrl: './aceptar-contrato.component.html',
  styles: [
  ]
})
export class AceptarContratoComponent implements OnInit {

  constructor(public dialog:MatDialog) { }

  close:boolean = false;

  okey(){
  this.close = !this.close;
  this.dialog.closeAll();
  }



  ngOnInit(): void {
  }

}
