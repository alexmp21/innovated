import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { ResumenContizacionComponent } from '../resumen-contizacion/resumen-contizacion.component';

@Component({
  selector: 'app-version-factesol',
  templateUrl: './version-factesol.component.html',
  styles: [
  ]
})
export class VersionFactesolComponent implements OnInit {

  constructor(public dialog:MatDialog) {}
  loading:boolean = false;
  loagingc(){
    this.loading =!this.loading;
    this.Settimeout_msexito();

  }


// ModalTime
Settimeout_msexito() {
  setTimeout(() => {
    this.loading = false;
    this.resumencotizacion()
  }, 2000);
}


resumencotizacion(){
  this.dialog.open(ResumenContizacionComponent);
}

  ngOnInit(): void {
  }

  // resaltado de botones de  selecion
tab  : any = 'tab1';
tab1 : any
tab2 : any
tab3 : any
Clicked : boolean

  onClick(check){
      if(check==1){
        this.tab = 'tab1';
      }else if(check==2){
        this.tab = 'tab2';
      }else{
        this.tab = 'tab3';
      }
  }



}
