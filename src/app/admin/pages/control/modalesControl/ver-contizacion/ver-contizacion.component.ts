import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { VersionFactesolComponent } from '../version-factesol/version-factesol.component';

@Component({
  selector: 'app-ver-contizacion',
  templateUrl: './ver-contizacion.component.html',
  styles: [
  ]
})
export class VerContizacionComponent implements OnInit {

  constructor(public dialog:MatDialog) { }

  // Modal de version factesol
versionfactesol(){
  this.dialog.open(VersionFactesolComponent);
}

  ngOnInit(): void {
  }

}
