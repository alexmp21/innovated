import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { VerContizacionComponent } from './modalesControl/ver-contizacion/ver-contizacion.component';
import { VerDetalleComponent } from './modalesControl/ver-detalle/ver-detalle.component';
import { VerificarComponent } from './modalesControl/verificar/verificar.component';


@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styles: [
  ]
})
export class ControlComponent implements OnInit {


  Dropdowns: boolean = false;


  Dropdownsfiltrar(){
    this.Dropdowns =!this.Dropdowns;
  }

  constructor(public dialog:MatDialog ) { }

  ngOnInit(): void {

  }


  // modal--Ver Detalle ..........
  verDetalle() {
    this.dialog.open(VerDetalleComponent);
  }

  // /modal de  ver contizacion
   verContizacion() {
     this.dialog.open(VerContizacionComponent);
   }


   verficacion(){
     this.dialog.open(VerificarComponent);
   }




}
