import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { ManualComponent } from './pages/manual/manual.component';
import { ControlComponent } from './pages/control/control.component';
import { ListadoComponent } from './pages/listado/listado.component';
import { DocumentosComponent } from './pages/listado/documentos/documentos.component';





const routes: Routes = [
  {
    path: '', component: AdminComponent,
    children: [
      { path: 'manual', component: ManualComponent },
      { path: 'control', component: ControlComponent },
      { path: 'listado', component: ListadoComponent },
      { path: 'documentos', component: DocumentosComponent },
      { path: '', redirectTo: '/login', pathMatch: 'full' },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class adminRoutingModule { }
