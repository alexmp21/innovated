import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {


  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  // sidebar

  tab : any = 'tab1';
  tab1 : any
  tab2 : any
  tab3 : any
  Clicked : boolean | undefined

  acordion:boolean = true;
  accordionsidebar(){
    this.acordion = !this.acordion;
  }


    onClick(check: number){
    //    console.log(check);
        if(check==1){
          this.tab = 'tab1';
        }else if(check==2){
          this.tab = 'tab2';
        }else{
          this.tab = 'tab3';
        }

}



  openTab1: boolean = false;
  openTab2: boolean = false;
  openTab3: boolean = false;
  openTab4: boolean = false;


  changetabs() {
    this.openTab1 = false;
    this.openTab2 = false;
    this.openTab3 = false;
    this.openTab4 = false;


}

toggleTabs($tabNumber: number) {
  this.changetabs();
  if ($tabNumber === 1) this.openTab1 = true;
  else if ($tabNumber == 2) this.openTab2 = true;
  else if ($tabNumber == 3) this.openTab3 = true;
  else if ($tabNumber == 4) this.openTab3 = true;


}

constructor(private router: Router) {
  const ruta = this.router.url;
  if (ruta.includes('manual')) this.openTab1 = true;
  else if (ruta.includes('control')) this.openTab2 = true;
  else if (ruta.includes('listado')) this.openTab3 = true;
  else if (ruta.includes('configuraciones')) this.openTab4 = true;

 }

  ngOnInit(): void {



  }
}
