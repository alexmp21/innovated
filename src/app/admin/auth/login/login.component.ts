import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  constructor(public spinn: NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinn.show();
    setTimeout(() => {
      this.spinn.hide();
    }, 3000);

  }

}
