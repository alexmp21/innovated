import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { adminRoutingModule } from './admin/admin.routing';
import { LoginComponent } from './admin/auth/login/login.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {path: '', redirectTo: '/login' , pathMatch: 'full'}

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,{ useHash: true }),
    adminRoutingModule
  ]
  ,
  exports: [RouterModule]
})
export class AppRoutingModule { }
